#Practico 1 - Vision por Computadora Año 2022
#Alumno: Barembaum Pablo
#Legajo: 72039

def adivina(intentos):      #Declaracion de la funcion
    print('\nUsted decidio adivinar en ' + intentos + ' intentos.')
    import random           #Traemos la libreria random
    numrand=random.randint(0,20)     #Llamamos a la funcion randint para elegir aleatoriamente un entero

    for i in range (int(intentos)): #Funcion range es el incrementador de i, por defecto arranca en 0 hasta el int intentos

        num=int(input("\nIngrese el numero a adivinar: "))
        #print(numrand) utilizado para debugear el codigo
        if num==numrand:    #En caso de acertar sale del if, mostrando la cantidad de intentos usados y los disponibles
            print("Felicitaciones, adivino el numero aleatorio!")   
            print('Lo hizo en ' , i+1 ,  'de los ' + intentos +  ' intentos elegidos.')
            break
        elif num<numrand:   #En caso de no acertar continua dentro del bucle for
            print("El numero secreto es mayor. Siga intentando")
        else:
            print("El numero secreto es menor. Siga intentando")

    if num!=numrand:
        print("\n\nLo lamento, usted llego al maximo numero de intentos elegido. El numero aleatorio era: ",numrand,"\n")    #Fin del bucle for alcanzando total de intentos

print("\nBIENVENIDO AL ADIVINADOR DE UN NUMERO ALEATORIO DE 0 A 20")

intentos= input("\nPara arrancar eliga el numero de intentos: ")    #Argumento a pasar a la funcion adivina 
adivina(intentos)