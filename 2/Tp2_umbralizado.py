# # Practico 2 - Vision por Computadora Año 2022
#Alumno: Barembaum Pablo
#Legajo: 72039
import cv2

# Leemos la imagen y obtenemos el tamaño

img = cv2.imread ('hoja.png', 0)
num_fila, num_columna = img.shape

#(0,0,0) el negro
#(255,255,255) el blanco

for i in range (1, num_fila):       #Ingresamos a la matriz por fila->columna y cambiamos el valor del pixel a 0=negro
    for j in range (num_columna):
        if img[i][j] < 190: # A partir de 190, que es practicamente negro se elige como  umbral para mandar a 0 un pixel
            img[i][j]=0
        else:
            img[i][j]=255 #Sino por defecto pasa el balor a blanco, pixel=255



cv2.imwrite('img_resultado.png', img) #Utilizando Ocv para escribir la imagen y manejo de la ventana
cv2.imshow('imagen resultado',img)
cv2.waitKey(0)
cv2.destroyAllWindows()
