# Practico 3 - Vision por Computadora Año 2022
#Alumno: Barembaum Pablo
#Legajo: 72039


import sys
import cv2
cap = cv2.VideoCapture('prueba_video.mp4') #Para capturar un video

fourcc = cv2.VideoWriter_fourcc(*'mp4v') #formato para codigo de cuatro caracteres

frameSize = (int(cap.get(cv2.CAP_PROP_FRAME_WIDTH)),int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT)))
fps = cap.get(cv2.CAP_PROP_FPS)
out = cv2.VideoWriter('prueba_video_gris.mp4',fourcc,fps,frameSize) 
#Argumentos cv2.VideoWriter()
#1.nombre del archivo de salida, 
#2.codificacion del video, 
#3.numero de cuadros por segundo (fps), 
#4.resolucion del video a guardar (W,H)=ancho,alto

print("FPS: ",fps)     
print("Size: ",frameSize) 

while(cap.isOpened()): #Con isOpened chequeamos que esté correctamente abierto
    ret,frame = cap.read()  #Con read() capturamos una imagen
    if ret == True:
        gray = cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY) #Gris
        out.write(gray) #Escribo en 'out', el color gris
        cv2.imshow('Video gris',gray) #Muestro el video en gris
        if cv2.waitKey(int(fps)) & 0xFF == ord('q'): #salgo con q
            break
    else:
        break
    
    #Extra - Girar/espejar la camara (flip)

    #if ret == True:
    #    frame = cv2.flip(frame,1)
    #    out.write(frame)
    #    cv2.imshow('Frame',frame)
    #    if cv2.waitKey(1) & 0xFF == ord('q'):
    #        break
    #else:
    #    break    


cap.release() #con release() cerramos la camara
out.release()
cv2.destroyAllWindows() 