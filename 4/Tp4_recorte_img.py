# Practico 4 - Vision por Computadora Año 2022
#Alumno: Barembaum Pablo
#Legajo: 72039

#Manipulación de imágenes
#A. Usando como base el programa anterior, escribir un programa que 
#   permita seleccionar una porción rectangular de una imagen, luego:
#   -con la letra "g" guardar la porción de la imagen seleccionada
#   como una nueva imagen.
#   -con la letra "r" restaurar la imagen original y permitir reaizar
#   una nueva selección.
#   -con la "q" finalizar.

import cv2
import numpy as np 

drawing = False  #verdadero si el click del mouse es presionado

ix,iy =-1,-1 #coordenadas iniciales 
fx,fy =-1,-1 #coordenadas finales 

def rectangulo(event,x,y,flags,param):
    global ix,iy,fx,fy,drawing
    
    if event == cv2.EVENT_LBUTTONDOWN: #evento inicial del mouse (click izquierdo)
        ix, iy = x, y #coordenadas iniciales
        drawing = True 
    elif event == cv2.EVENT_MOUSEMOVE: 
        if drawing is True:            
            fx, fy = x, y #coordenadas finales
            cv2.rectangle(img_copy2,(ix,iy),(fx,fy),(255,0,0),thickness=4)
    elif event == cv2.EVENT_LBUTTONUP: #evento final del mouse (suelto click izquierdo)
        fx, fy = x, y #coordenadas finales
        drawing = False
        cv2.rectangle(img,(ix,iy),(fx,fy),(0,255,0),thickness=4) #rectangulo verde

img = cv2.imread('Imagen_original.png',1) #leo la imagen .png
img_copy1 = img.copy()           #copio la imagen en una variable copy  
img_copy2 = img.copy()            #almaceno la imagen en una variable auxiliar para manipular

print('Dibuje un rectángulo con el mouse para recortar la imagen\n')
print('Presione  G para guardar el recorte\n')
print('R para restaurar la imagen original\n')
print('Q para salir')
cv2.namedWindow('Imagen a recortar')  
cv2.setMouseCallback('Imagen a recortar',rectangulo) 


while(1): 
    cv2.imshow('Imagen a recortar',img) #muestro la imagen leida anteriormente
    k = cv2.waitKey(1) & 0xFF
    if k == ord ('g'): #'g' para guardar el recorte
        img_recortada = img[(iy+5):(fy-5),(ix+5):(fx-5)] #evito rectangulo de color en la imagen cortada
        cv2.imwrite('Imagen_recortada.png',img_recortada)
        cv2.imshow('Imagen recortada',img_recortada) #muestro la imagen cortada (aparte de la original)
    elif k == ord('r'): #'r' para restaurar la imagen original
        img = img_copy1.copy() #Se guarda la imagen original para restaurarla en caso deseado
        cv2.destroyWindow('Imagen a recortar') #cuando restauro, se cierra la ventana de la imagen cortada
    elif k == ord('q'): #'q' para salir del programa
        break

cv2.destroyAllWindows()