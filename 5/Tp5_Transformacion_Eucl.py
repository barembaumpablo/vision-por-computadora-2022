# Practico 5 - Vision por Computadora Año 2022
#Alumno: Barembaum Pablo
#Legajo: 72039

#Rotación + Traslación (o Transformación Euclidiana)
#A. Crear un método que aplique una transformación euclidiana, recibiendo
#   los siguientes parámetros:
#                               -angle: ángulo
#                               -tx: traslación en x
#                               -ty: traslación en y
#
#B. Usando como base el programa anterior, escribir un programa que 
#   permita seleccionar una porción rectangular de una imagen, luego:
#   -con la letra "e" aplique una transformación euclidiana a la porción
#   de imagen seleccionada y la guarde como una nueva imagen.
import cv2
import numpy as np 
import math

drawing = False  #verdadero si el click del mouse es presionado


ix,iy =-1,-1 #coordenadas iniciales 
fx,fy =-1,-1 #coordenadas finales 

def rectangulo(event,x,y,flags,param):
    global ix,iy,fx,fy,drawing
    
    if event == cv2.EVENT_LBUTTONDOWN: #evento inicial del mouse (click izquierdo)
        ix, iy = x, y #coordenadas iniciales
        drawing = True 
    elif event == cv2.EVENT_MOUSEMOVE: 
        if drawing is True:            
            fx, fy = x, y #coordenadas finales
            cv2.rectangle(img_copy2,(ix,iy),(fx,fy),(255,0,0),thickness=4)
    elif event == cv2.EVENT_LBUTTONUP: #evento final del mouse (suelto click izquierdo)
        fx, fy = x, y #coordenadas finales
        drawing = False
        cv2.rectangle(img,(ix,iy),(fx,fy),(0,255,0),thickness=4) #rectangulo verde

def trans_euclidiana(img,rows,columns,tx,ty,angle):
    (rows,columns) = (img.shape[0],img.shape[1])

    rad_angle = math.radians(angle)

    M = np.float32([[np.cos(rad_angle),np.sin(rad_angle),tx],
                    [-np.sin(rad_angle),np.cos(rad_angle),ty]])
    
    shifted = cv2.warpAffine(img,M,(columns,rows))
    return shifted


img = cv2.imread('Imagen_original.png',1) #leo la imagen .png
img_copy1 = img.copy()           
img_copy2 = img.copy()            

print('Transformación euclidiana.\n')
print('1 : Realice un rectángulo para recortar la imagen.\n')
print('2 : Presione la tecla "G" para guardar el recorte.\n')
print('3 : Presione "E" para realizar la transformada.\n')
print('"R" para restaurar la imagen\n')
print('"Q" para salir del programa')
cv2.namedWindow('Imagen a recortar')  
cv2.setMouseCallback('Imagen a recortar',rectangulo) 
flag=0


while(1): 
    cv2.imshow('Imagen a recortar',img)
    k = cv2.waitKey(1) & 0xFF
    if k == ord ('g'): 
        img_recortada = img[(iy+5):(fy-5),(ix+5):(fx-5)] 
        cv2.imwrite('Imagen_recortada.png',img_recortada)
        cv2.imshow('Imagen recortada',img_recortada) #muestro la imagen cortada (aparte de la original)
        flag=1
    elif (k == ord ('e')):
        if flag == 1:
            (rows,columns) = (img_recortada[0],img_recortada[1])
            print('\nIngrese ángulo de rotación: ')
            angle = int(input())
            print('Ingrese traslación en el eje x: ')
            tx = int(input())
            print('Ingrese traslación en el eje y: ')
            ty = int(input())
            img_euclidiana = trans_euclidiana(img_recortada,rows,columns,tx,ty,angle)
            cv2.imwrite('Imagen Euclidiana.png',img_euclidiana)
            cv2.imshow('Imagen Recortada',img_recortada)
            cv2.imshow('Imagen Euclidiana', img_euclidiana)
            print('Imagen desplegada, Presione 1 para finalizar')
            salir = int(input())
            if salir==(1):
                break
        else:
            print('******Primero recorte la imagen.******')
            continue
    elif k == ord('r'): 
        img = img_copy1.copy() #Se guarda la imagen original para restaurarla en caso deseado
        cv2.destroyWindow('Imagen a recortar')
    elif k == ord('q'):
        break

cv2.destroyAllWindows()