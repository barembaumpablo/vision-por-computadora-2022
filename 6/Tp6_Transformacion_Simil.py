# Practico 6 - Vision por Computadora Año 2022
#Alumno: Barembaum Pablo
#Legajo: 72039

#Transformación de similaridad
#A. Agregar al método anterior un parámetro que permita aplicar un escalado a la porción,
#   rectangular de imagen:
#                       Parametros:
    #                               -angle: ángulo
    #                               -tx: traslación en x
    #                               -ty: traslación en y
    #                               -s: escala
#
#B. Usando como base el programa anterior, escribir un programa que 
#   permita seleccionar una porción rectangular de una imagen, luego:
#   -con la letra "s" aplique una transformación de similaridad a la porción
#   de imagen seleccionada y la guarde como una nueva imagen.


import cv2
import numpy as np 
import math


drawing = False  #verdadero si el click del mouse es presionado

ix,iy =-1,-1 #coordenadas iniciales 
fx,fy =-1,-1 #coordenadas finales 

def rectangulo(event,x,y,flags,param):
    global ix,iy,fx,fy,drawing
    
    if event == cv2.EVENT_LBUTTONDOWN: #evento inicial del mouse (click izquierdo)
        ix, iy = x, y #coordenadas iniciales
        drawing = True 
    elif event == cv2.EVENT_MOUSEMOVE: 
        if drawing is True:            
            fx, fy = x, y #coordenadas finales
            cv2.rectangle(img_copy2,(ix,iy),(fx,fy),(255,0,0),thickness=4)
    elif event == cv2.EVENT_LBUTTONUP: #evento final del mouse (suelto click izquierdo)
        fx, fy = x, y #coordenadas finales
        drawing = False
        cv2.rectangle(img,(ix,iy),(fx,fy),(0,255,0),thickness=4) #rectangulo verde

def T_similaridad(img,rows,columns,tx,ty,angle,s):
    (rows,columns) = (img.shape[0],img.shape[1])

    rad_angle = math.radians(angle)


    M = np.float32([[s*np.cos(rad_angle),s*np.sin(rad_angle),tx],
                    [s*(-np.sin(rad_angle)),s*np.cos(rad_angle),ty]])

    shifted = cv2.warpAffine(img,M,(columns,rows))
    return shifted

img = cv2.imread('Imagen_original.png',1) #leo la imagen .png
img_copy1 = img.copy()           
img_copy2 = img.copy()            

print('Transformación Similaridad.\n')
print('1 : Realice un rectángulo para recortar la imagen.\n')
print('2 : Presione la tecla "G" para guardar el recorte.\n')
print('3 : Presione "S" para realizar la transformada.\n')
print('"R" para restaurar la imagen\n')
print('"Q" para salir del programa')
cv2.namedWindow('Imagen')  
cv2.setMouseCallback('Imagen',rectangulo) 



while(1): 
    cv2.imshow('Imagen',img)
    k = cv2.waitKey(1) & 0xFF
    if k == ord ('g'): 
        img_recortada = img[(iy+5):(fy-5),(ix+5):(fx-5)] 
        cv2.imwrite('Imagen recortada.png',img_recortada)
        cv2.imshow('Imagen recortada',img_recortada) #muestro la imagen cortada (aparte de la original)
        flag=1
    elif (k == ord ('s')):
        if flag == 1:
            (rows,columns) = (img_recortada.shape[0],img_recortada.shape[1])
            print('\nIngrese ángulo de rotación: ')
            angle = int(input())
            print('Ingrese traslación en el eje x: ')
            tx = int(input())
            print('Ingrese traslación en el eje y: ')
            ty = int(input())
            print('Ingrese el factor de escala isotrópico S: ')
            s = float(input())
            img_similarizada = T_similaridad(img_recortada,rows,columns,tx,ty,angle,s)
            cv2.imwrite('Imagen Similarizada.png',img_similarizada)
            cv2.imshow('Imagen Recortada',img_recortada)
            cv2.imshow('Imagen Similarizada',img_similarizada)
            
            print('Imagen desplegada, Presione 1 para finalizar')
            salir = int(input())
            if salir==(1):
                break
        else:
            print('******Primero recorte la imagen.******')
            continue
    elif k == ord('r'): 
        img = img_copy1.copy() #Se guarda la imagen original para restaurarla en caso deseado
        cv2.destroyWindow('Imagen recortada')
        cv2.destroyWindow('Imagen Similarizada')
    elif k == ord('q'):
        break

cv2.destroyAllWindows()