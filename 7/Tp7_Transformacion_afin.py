# Práctico 7 - Vision por Computadora Año 2022
#Alumno: Barembaum Pablo
#Legajo: 72039

#Transformación afín - Incrustando imágenes
#
#Teniendo en cuenta que:
#                       -una transformación afín se representa con una 
#                       matriz de 2x3 (6 grados de libertad).
#                       -puede ser recuperada con 3 puntos no colineales
#A. Crear un método que compute la transformación afín entre 3 pares de 
#puntos correspondientes.
#B. Usando como base el programa anterior, escriba un programa que
#   -con la letra "a" permita seleccionar con el mouse 3 puntos no 
#   colineales en una imagen e incruste entre estos puntos seleccionados 
#   una segunda imagen.
#
#Ayuda:
#       -cv2.getAffineTransform
#       -cv2.warpAffine
#       -generar una máscara para insertar una imagen en otra


import cv2
import numpy as np
import math
#import matplotlib as plt
#import imutils

punto0 = [[-1, -1], [-1, -1], [-1, -1]] 
i = 0

def select_3_puntos(event,x,y,flags,param):
    global punto0, punto1, i

    if event == cv2.EVENT_LBUTTONUP:
        if i == 0:
            punto0[0] = x,y
            cv2.circle(img_copy2,(x,y),3,(0,255,0),-1)
            i = i+1
        elif i == 1:
            punto0[1] = x,y
            cv2.circle(img_copy2,(x,y),3,(0,255,0),-1)
            i = i+1
        elif i == 2:
            punto0[2] = x,y
            cv2.circle(img_copy2,(x,y),3,(0,255,0),-1)
            i = 0


def transformacion_afin(img):
    (rows,columns) = (img.shape[0],img.shape[1])
    origen_puntos = np.float32([[0,0],[0,rows-1],[columns-1,rows-1]]) #esquinas superior izquierda - superior derecha - inferior derecha
    destino_puntos = np.float32([[punto0[0],punto0[1],punto0[2]]]) #esquinas superior derecha - inferior derecha - inferior izquierda
    M = cv2.getAffineTransform(origen_puntos,destino_puntos) #matriz entre los puntos origen y destino
    img_transformada = cv2.warpAffine(img,M,(columns,rows))
    return img_transformada


def mascara_img(imagen_transformada,img_copy1):
    mascara = imagen_transformada.copy() #copio en una máscara el volante incrustado en la img_copy2 de la 'transformacion_afin'
    mascara[mascara != 0] = 255  #los valores en que máscara es distinta de cero (distinta de negro), coloco en 255 dichos valores 
    mascara = cv2.bitwise_not(mascara) #cv2.bitwise_not me lleva los valores de 0 a 255 y los valores de 255 a 0 (operación not) de la máscara
    mascara_final = cv2.bitwise_and(mascara, img_copy1) #cv2.bitwise_and : me lleva a 255 sólo los valores que son 255 en máscara y en img_copy1,
    #                                                                     los demas valores todos en 0.
     # mascara - img_copy1 | mascara_final
      #   0           0    |      0
      #   0           255  |      0
      #   255         0    |      0
      #   255         255  |      255
      
    return mascara_final


img = cv2.imread('Imagen_original.png',1) #leo la imagen .jpg
img_volante = cv2.imread('Volante.png',1) #leo imagen a incrustar

#Dimensiones imagen 1, para redimensionar imagen 2 (para incrustar)
(rows,columns) = (img.shape[0],img.shape[1])
img_volante = cv2.resize(img_volante,(columns,rows))
#cv2.imshow('Sombrero', img_volante)

img_copy1 = img.copy()           
img_copy2 = img.copy()            

print('Transformación afín.')
print('Elija 3 puntos para insertar en el orden:\n')
print('1° Esquina superior derecha\n')
print('2° Esquina inferior derecha\n')
print('3° Esquina inferior izquierda\n')
print('-Luego, presione "A" para realizar la transformada afín\n')
print('"R" para restaurar la imagen\n')
print('"Q" para salir del programa.')
cv2.namedWindow('Imagen')  
cv2.imshow('Imagen', img_copy2)
cv2.setMouseCallback('Imagen',select_3_puntos) #llamo a la función 'selec_3_puntos'

while(1): 
    
    cv2.imshow('Imagen',img_copy2) 
    k = cv2.waitKey(1) & 0xFF
    if k == ord('a'):
        imagen_transformada = transformacion_afin(img_volante) #obtengo la imagen transformada a través de una función de transformada afín
        mascara_volante = mascara_img(imagen_transformada,img_copy1) 
        img_transformada = cv2.bitwise_or(mascara_volante, imagen_transformada) #cv2.bitwise_or: me lleva todos los valores a 255, menos en los que
         #                                                                                 en ambas imagenes son 0.
         # mascara_volante - imagen_transformada | img_transformada
         #   0               0                   |      0
         #   0               255                 |      255
         #   255             0                   |      255
         #   255             255                 |      255

        cv2.imwrite('Imagen_transformada.png', img_transformada)
        cv2.namedWindow('Imagen_transformada')
        cv2.imshow('Imagen_transformada', img_transformada)

    elif k == ord('r'): 
        img_copy2 = img_copy1.copy()
        cv2.destroyWindow('Imagen_transformada') #al restaurar, se cierra la ventana de la imagen con transformación afín
    elif k == ord('q'): 
        break

cv2.waitKey(0) 
cv2.destroyAllWindows()