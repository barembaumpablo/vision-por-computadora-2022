# Práctico 8 - Vision por Computadora Año 2022
#Alumno: Barembaum Pablo
#Legajo: 72039

#Rectificando imagenes
#
#Teniendo en cuenta que:
#                       -una homografía se representa con una matriz de 3x3
#                       (pero tiene sólo 8 grados de libertad)
#                       -puede ser recuperada con 4 puntos no colineales.
#A. Crear un método que compute la homografía entre los 4 pares de puntos
#   correspondientes.
#B. Usando como base el programa anterior, escriba un programa que
#   -con la letra "h" permita seleccionar con el mouse 4 puntos no
#   colineales en una imagen y transforme (rectifique) la selección
#   en una nueva imagen rectangular.
#
#Ayuda:
#       -cv2.getPerspectiveTransform
#       -cv2.warpPerspective


import cv2
import numpy as np
import math


drawing = False #verdadero si el click del mouse es presionado

punto0 = [[-1, -1], [-1, -1], [-1, -1],[-1,-1]] 

i = 0

def selecc_4puntos(event,x,y,flags,param): #seleccionamos 4 puntos en un orden espcífico para graficar
    global punto0, punto1, i, flag

    if event == cv2.EVENT_LBUTTONUP:
        if i == 0:
            punto0[0] = x,y
            cv2.circle(img_copy2,(x,y),3,(0,255,0),-1)
            i = i + 1
        elif i == 1:
            punto0[1] = x,y
            cv2.circle(img_copy2,(x,y),3,(0,255,0),-1)
            i = i + 1
        elif i == 2:
            punto0[2] = x,y
            cv2.circle(img_copy2,(x,y),3,(0,255,0),-1)
            i = i + 1
        elif i == 3:
            punto0[3] = x,y
            cv2.circle(img_copy2,(x,y),3,(0,255,0),-1)
            flag = 1  

def distancia(p1, p2):  #Devuelve el escalar de la distancia entre dos puntos
	a = p2[0] - p1[0]
	b = p2[1] - p1[1]
	return np.sqrt((a ** 2) + (b ** 2))

def transformacion_homografica(img_copy2):

    top_left, top_right, bottom_right, bottom_left = punto0[0], punto0[1], punto0[2], punto0[3] #4 puntos seleccionados en la imagen
    src = np.array([top_left, top_right, bottom_right, bottom_left], dtype='float32') #coloco los 4 puntos en un arreglo llamado 'src'
    rows = int(max(distancia(bottom_right, top_right),distancia(top_left, bottom_left))) #Máximo valor numérico de filas (entre lado derecho y lado izquierdo)
    columns = int(max(distancia(top_left, top_right),distancia(bottom_right, bottom_left))) #Máximo valor numérico de columnas (entre lado superior y lado inferior)
   

     #Promedio del tamaño de todos los lados
    side = int(max([
		distancia(bottom_right, top_right),
		distancia(top_left, bottom_left),
		distancia(bottom_right, bottom_left),
		distancia(top_left, top_right)]))
        
    
    dst = np.array([[0, 0], [columns - 1, 0], [columns - 1, rows - 1], [0, rows - 1]], dtype='float32') #arreglo 'dst' para los 4 puntos 
    map_matrix = cv2.getPerspectiveTransform(src, dst)      #Matriz entre los puntos de 'src' y 'dst'
    img_perspective = cv2.warpPerspective(img_copy2, map_matrix, (int(columns), int(rows))) #Obtengo imagen seleccionada por puntos en perspectiva

    return img_perspective 
    
#///////////////////////////////////////////////#

img = cv2.imread('Farmacia.png',1) 
img_copy1 = img.copy()           
img_copy2 = img.copy()            

print('Transformación homografía.\n')
print('Elija 4 puntos para ver la perspectiva en el orden:')
print('1° Esquina superior izquierda')
print('2° Esquina superior derecha')
print('3° Esquina inferior derecha')
print('4° Esquina inferior izquierda\n')
print('Presione la tecla "H" para realizar la transformada.\n')
print('"R" para restaurar la imagen\n')
print('"Q" para salir del programa.')

cv2.namedWindow('Imagen original')  #nombre de la ventana
cv2.setMouseCallback('Imagen original',selecc_4puntos) 

while(1): #while infinito
    cv2.imshow('Imagen original',img_copy2) #muestro la imagen leida anteriormente
    k = cv2.waitKey(1) & 0xFF
    if k == ord('h'):
        img_transformada = transformacion_homografica(img_copy2) #obtengo la Imagen transformada a través de una función pasandole la 'img_copy2' 
        cv2.imwrite('Farmacia transformada.png', img_transformada)
        cv2.namedWindow('Imagen transformada')
        cv2.imshow('Imagen transformada', img_transformada)
        i = 0 #coloco en 0 la variable 'i' para la selección de puntos
    elif k == ord('r'): 
        img_copy2 = img_copy1.copy()
        cv2.destroyWindow('Imagen transformada') #cuando restauro, se cierra la ventana de la imagen con transformación homográfica
    elif k == ord('q'): #'q' para salir del programa
        break

cv2.waitKey(0) 
cv2.destroyAllWindows()