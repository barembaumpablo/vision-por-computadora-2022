# Práctico 9 - Vision por Computadora Año 2022
#Alumno: Barembaum Pablo
#Legajo: 72039

#Medición de objetos
#Imagen -> fachada de una casa
#
#Como dato de entrada, sabemos que el tamaño del marco de la puerta es de
#2.10m de alto por 0.73m de ancho. En base a estas medidas hacer un programa
#que permita medir el largo de cosas en el plano de la fachada.
#
#Capturar una imagen de una fachada con perspectiva y hacer un programa que
#permita:
#       -encontrar la transformación perspectiva entre los 4 vertices del
#       marco y un rectángulo con la misma relación de aspecto que la
#       puerta real.
#       -aplicar dicha transformación a la imagen y mostrarla en una ventana.
#       -sobre esta ventana permitir que el usuario haga dos clicks y mostrar
#       la distancia en metros entre dichos puntos.
#       -permitir que cuando se presione la letra 'r' se reinicie la medición.
#       -por último medir dos objetos en la imagen (ventana,casilla de gas,etc)
#       y comparar los resultadoados con la medida real.
#

import cv2
import numpy as np
from math import sqrt

drawing = False #verdadero si el click del mouse es presionado

punto0 = [[52,7], [1158, 9], [1160, 768],[21,712]]
puerta = [[325,165], [720, 144], [710, 723],[325,707]]
punto1 = [[-1, -1], [-1, -1]]
punto2 = [[-1, -1], [-1, -1], [-1, -1],[-1,-1]]
i = 0

def selecc_medidas(event,x,y,flags,param):
    global punto1, i, k, flag, flag1, drawing
#Utiliza pixeles de la puerta determinados previamente y calcula la distancias entre puntos
    if event == cv2.EVENT_RBUTTONDBLCLK:
        print('Las medidas de la puerta son: \n')
        medida1 = distancia_pixel(325,165,720, 144)
        medida2 = distancia_pixel(720, 144,710, 723)
        cv2.line(img_medida, (325,165), (720, 144), (0,0,255),1)
        cv2.line(img_medida, (720, 144), (710, 723), (0,0,255),1)
        cv2.putText(img_medida, medida1, (385, 135), cv2.FONT_HERSHEY_SCRIPT_SIMPLEX, 2, (0,0,255), 2 , cv2.LINE_8)
        cv2.putText(img_medida, medida2, (360, 400), cv2.FONT_HERSHEY_SCRIPT_SIMPLEX, 2, (0,0,255), 2 , cv2.LINE_8)
        cv2.circle(img_medida,(325,165), 3, (0,0,255), -1)
        cv2.circle(img_medida,(720, 144), 3, (0,0,255), -1)
        cv2.circle(img_medida,(710, 723), 3, (0,0,255), -1)
        cv2.circle(img_medida,(325,707), 3, (0,0,255), -1)
        img_transformada[:] = img_medida[:]

#Medicion entre 2 puntos seleccionados utilizando la misma escala que en la puerta
    elif event == cv2.EVENT_LBUTTONDOWN:
        drawing = True
        punto1[i] = x,y
        if i == 0:
            print('Seleccione con click izquierdo el 1° punto a medir.\n')
            print('Punto inicio de la medida: ',i,'-> ',punto1[i])
        else:
            print('Punto final de la medida: ',i,'-> ',punto1[i])
        i = i + 1

        if i == 2:
            cv2.line(img_medida,(punto1[0][0],punto1[0][1]),(punto1[1][0],punto1[1][1]),(0,0,255),1)
            img_transformada[:] = img_medida[:]
            drawing = False
            i = 0
    
    elif event == cv2.EVENT_MOUSEMOVE:
        if (drawing is True):
            img_medida[:] = img_transformada[:]
            medida = distancia_pixel(punto1[0][0],punto1[0][1],x,y)
            cv2.line(img_medida, (punto1[0][0],punto1[0][1]), (x,y), (0,0,255),1)
            cv2.putText(img_medida, medida, (x+10, y+10), cv2.FONT_HERSHEY_SCRIPT_SIMPLEX, 1, (0,0,255), 2 , cv2.LINE_AA)

def distancia_pixel(px1, py1, px2, py2):
	#Devuelve en metros la distancia entre dos pixeles
    px = px2 - px1
    py = py2 - py1
    #Factor de escala para medicion en ambos ejes 
    px_cm = px*(0.5/140) 
    py_cm = py*(0.5/140) 
    resultado = sqrt((px_cm ** 2) + (py_cm ** 2))
    resultado = round(resultado,2)
    resultado = str(resultado)

    return (resultado + ' metros')


def distancia_2puntos(p1, p2):
	#Devuelve el valor absoluto de la distancia entre dos puntos
	a = p2[0] - p1[0]
	b = p2[1] - p1[1]
	return np.sqrt((a ** 2) + (b ** 2))

def transformacion_homografica(img_copy2):
    top_left, top_right, bottom_right, bottom_left = punto0[0], punto0[1], punto0[2], punto0[3]

    src = np.array([top_left, top_right, bottom_right, bottom_left], dtype='float32')
    rows = int(max(distancia_2puntos(bottom_right, top_right),distancia_2puntos(top_left, bottom_left)))
    columns = int(max(distancia_2puntos(top_left, top_right),distancia_2puntos(bottom_right, bottom_left)))
    side = max([
		distancia_2puntos(bottom_right, top_right),
		distancia_2puntos(top_left, bottom_left),
		distancia_2puntos(bottom_right, bottom_left),
		distancia_2puntos(top_left, top_right)
	])
    dst = np.array([[0, 0], [columns - 1, 0], [columns - 1, rows - 1], [0, rows - 1]], dtype='float32')
    map_matrix = cv2.getPerspectiveTransform(src, dst)
    img_perspectiva = cv2.warpPerspective(img_copy2, map_matrix, (int(columns), int(rows)))
    
    return img_perspectiva 


img = cv2.imread('Fachada_original.jpeg',1) 
img_copy1 = img.copy()             
img_copy2 = img.copy()            
img_copy3 = np.zeros((img.shape[0], img.shape[1], img.shape[2]), np.uint8)

print('\n\nMedición de objetos\n')
print('-Presione doble click derecho para mostrar las medidas de la puerta\n')
print('-Presione click izquierdo para medir diferentes objetos\n')
print('Presione "O" para mostrar la fachada de la casa original.\n')
print('Presione "R" para restaurar la imagen\n')
print('Presione "Q" para salir del programa.')
cv2.imwrite('Fachada_original.jpeg', img_copy2)
img_transformada = transformacion_homografica(img_copy2) #convierto la imagen original->perspectiva antes de las mediciones
img_transformada_aux = img_transformada.copy()
img_medida = img_transformada.copy()
cv2.imwrite('Fachada_medida.jpeg', img_medida)
cv2.namedWindow('Fachada en perspectiva')
cv2.imshow('Fachada en perspectiva', img_transformada)
cv2.setMouseCallback('Fachada en perspectiva',selecc_medidas) 

while True: 
    cv2.imshow('Fachada en perspectiva', img_medida) #muestro la imagen leida anteriormente
    k = cv2.waitKey(1) & 0xFF
    if k == ord('g'):
        cv2.imwrite('Fachada_medida.jpeg',img_medida)
        cv2.destroyWindow('Fachada en perspectiva')
        fachada_medida = cv2.imread('Fachada_medida.jpeg',1)
        cv2.imshow('Fachada medida', fachada_medida)
        break
    elif k == ord('o'): #'o' para mostrar la imagen original de la fachada
        cv2.destroyWindow('Fachada en perspectiva')
        cv2.imshow('Fachada de la casa original',img_copy2)
        break
    elif k == ord('r'): #'r' para restaurar la imagen en perspectiva
        img_transformada = img_transformada_aux.copy()
        img_medida = img_transformada_aux.copy()
        img_copy2 = img_copy1.copy()
        i = 0
    elif k == ord('q'): 
        break

cv2.waitKey(0)
cv2.destroyAllWindows()